package org.dynamo.tests;

import java.math.BigDecimal;

public record RecordExample(String name, int age, boolean sex, BigDecimal salary) {

}
